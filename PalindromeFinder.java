// Найти слова палиндромы

import java.util.Scanner;

public class PalindromeFinder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку:");
        String text = scanner.nextLine();
        String reverseWord = "";

        System.out.println("Слова палиндромы:");
        for (String word: text.split(" ")) {
            for (int i = word.length() - 1; i >= 0; i--) {
                reverseWord = reverseWord.concat(String.valueOf(word.charAt(i)));
            }

            if (word.equalsIgnoreCase(reverseWord)) {   // если учитывать регистр, то нужно заменить условие на word.equals(reverseWord))
                System.out.println(word);
            }
            reverseWord = "";
        }
    }
}
