/*
- Введите любую строку. Замените в строке каждое второе вхождение «object-oriented programming» (не учитываем регистр символов) на «OOP»
- Например, строка
    "Object-oriented programming is a programming language model organized around objects rather than "actions" and data rather than logic.
    Object-oriented programming blabla.
    Object-oriented programming bla."
    должна быть преобразована в
    "Object-oriented programming is a programming language model organized around objects rather than "actions" and data rather than logic.
    OOP blabla.
    Object-oriented programming bla."
 */

import java.util.Scanner;

public class ReplaceEverySecondOccurrence {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку:");
        String text = scanner.nextLine();
        String newText = "";
        String occurrence = "Object-oriented programming", inset = "OOP";
        int textLength = text.length(), occurrenceLength = occurrence.length();
        char[] foundCharSequence = new char[occurrence.length()];
        int occurrenceNumber = 0;
        String foundSting;

        for (int i = 0; i < textLength; i++) {
            if (i + occurrenceLength <= textLength) {
                text.getChars(i, i + occurrenceLength, foundCharSequence, 0);
                foundSting = String.valueOf(foundCharSequence);

                if (occurrence.equalsIgnoreCase(foundSting)) {
                    occurrenceNumber++;
                    if (occurrenceNumber % 2 == 0) {
                        newText = newText.concat(inset);
                        i += occurrenceLength;
                    }
                }
            }
            newText = newText.concat(String.valueOf(text.charAt(i)));
        }
        System.out.println(newText);
    }
}
