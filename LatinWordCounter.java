// Найти количество слов, содержащих только символы латинского алфавита

import java.util.Scanner;

public class LatinWordCounter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку:");
        String text = scanner.nextLine();
        int countOfLatinWord = 0;

        for (String word: text.split(" ")) {
            if (word.matches("^[a-zA-Z]*$")) {
                countOfLatinWord++;
            }
        }
        System.out.println("Количество слов, содержащих только символы латинского алфавита: " + countOfLatinWord);
    }
}
