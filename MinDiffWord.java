/*
- Найти слово, в котором число различных символов минимально
- Слово может содержать буквы и цифры
- Если таких слов несколько, найти первое из них
- Например, в строке "fffff ab f 1234 jkjk" найденное слово должно быть "fffff"
 */

import java.util.Scanner;

public class MinDiffWord {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку:");
        String text = scanner.nextLine();
        String minDiffWord = null;
        int minDiff = 0;
        int countOfUniqueChars;
        char[] charSequence;

        for (String word: text.split(" ")) {
            countOfUniqueChars = word.length();
            charSequence = word.toCharArray();
            for (int i = 0; i < word.length(); i++) {
                if (i != word.indexOf(charSequence[i])) {
                    countOfUniqueChars--;
                }
            }

            if (minDiffWord == null) {
                minDiff = countOfUniqueChars;
                minDiffWord = word;
            } else if (countOfUniqueChars < minDiff) {
                minDiff = countOfUniqueChars;
                minDiffWord = word;
            }
        }
        System.out.println("Найденное слово: " + minDiffWord);
    }
}
